#!/usr/bin/env python
from __future__ import print_function
try:
    import Tkinter as tk #py2.7
    from tkFileDialog import askopenfilename, askdirectory
except ImportError:
    import tkinter as tk  #py3.4
    from tkinter.filedialog import askopenfilename, askdirectory


import os.path
import sys
import pandas as pd

""" 
APP:         TRANSFORM HLA
DESCRIPTION: Transform HLA data
CLASS: datamunging
  def: wide_to_long(self, filepath, savepath, prefix=None, dropchar = 1)

NOTE: first column must be sampleID

Author:  Lewis <lewis.r.liu@gmail.com>
Created: July 22, 2015
"""

#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+

class datamunging(object):
    def __init__(self):
        pass
    

    def readTable(self, filepath):        
        try:
            open(filepath)
        except IOError:
            print("file does not exist at '" + filepath +"'")
            return
        self.filepath = filepath

        #+------------+
        #| read table |
        #+------------+  
        print('READING TABLE: ' + str(filepath))        
        raw = pd.read_table(filepath)

        #+----------------------------------------+    
        #| categorical datatype for all but col 0 |
        #+----------------------------------------+
        hla_list = list(raw.columns)[1:] 
        for x in hla_list:
            raw[x] = raw[x].astype('category')
    
        #+----------------+
        #| print & return |
        #+----------------+
        print('\nREAD\n'); print(raw.shape); print(raw.head()); print('\n') 
        self.rawdata = raw
        return self
    
    def transform(self, filepath, prefix=None, dropchar = 1):
        self.readTable(filepath)
        raw = self.rawdata   
        #+----------------------+
        #| Exception handling   |
        #+----------------------+  
        if len(dropchar) is 0 or dropchar is None:
            dropchar = 1   
        
        if prefix is None or len(prefix) is 0:
            try:
                prefix = raw.columns[1][:-int(dropchar)]
            except:
                print('invalid raw.col or prefix')
                print('raw.columns[1]', raw.columns[1])
                print('prefix', raw.columns[1][:-dropchar])

        #+------------------------+
        #| split-apply-combine    |
        #+------------------------+ 
        df = pd.melt(raw
                       , id_vars=[raw.columns[0]]
                       , var_name = prefix)


        transformed = df.groupby([raw.columns[0], 'value']).count().unstack().fillna(0)    

        #transformed.columns = transformed.columns.droplevel()
        #return transformed

        #+---------------------------------------+
        #| drop multi-index and append prefix    |
        #+---------------------------------------+         
        # drop multi-level 0
        transformed.columns = transformed.columns.droplevel()
        oldcols = list(transformed.columns)

        # append prefix
        newcols = []
        for x in oldcols:
            new_colname = str(prefix) + "-" + str(x)
            newcols.append(new_colname)

        # replace newcols 
        transformed.columns = newcols
        final = transformed

        print('\nTRANSFORMED\n'); print(final.shape); print(final.head()); 
        self.final = final
        return self.final
    
    def save(self, savepath):
        self.final.to_csv(savepath, sep='\t', encoding='utf-8')
        print('SAVING TO: ' + savepath)
        return self 
    
    def wide_to_long(self, filepath, savepath, prefix=None, dropchar = 1):
        self.transform(filepath, prefix, dropchar)
        self.save(savepath)
        
#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+
       
class Redirector(object):
    """Redirects output from interpreter to GUI"""
    def __init__(self,text_widget):
        self.text_space = text_widget

    def write(self,string):
        self.text_space.insert('end', string)
        self.text_space.see('end')
    
    def flush(self):
        pass  # needed to close app()
              # else 'AttributeError: 'Redirector' object has no attribute 'flush'


#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+

    
class App(datamunging):
    """ GUI interface"""
    def __init__(self):
        self.root = tk.Tk()
        self.log = tk.Text(self.root)
        self.GUI()
        self.main()
        
    def GUI(self): 
        self.root.wm_title("Transform HLA data")
        #+-----------------------| 
        #| * Placeholder classes |
        #+-----------------------+
        
        UI = type('UI', (object,), {}) #empty class to hold attributes - http://tinyurl.com/5wkwgnv
        
        #+-----------------------+
        #| * UI elements - head  |
        #+-----------------------+
        
        UI.head = type('head', (object,), {}) #head class to hold UI attributes
        
        UI.head.frame = tk.Frame(self.root)        
        UI.head.frame.pack()
        
        UI.head.label = tk.Label(self.root, text="Updated: July 23, 2015\nCreated:  July 22, 2015\n lewis.r.liu@gmail.com\n")
        UI.head.label.pack(in_=UI.head.frame, side="left")
              
        
        #+--------------------------------+
        #| * UI elements - pick/read file |
        #+--------------------------------+   
        #| datamunging.readTable(filepath)
        
        UI.filepath = type('filepath', (object,), {}) 
        #view
        UI.filepath.frame = tk.Frame(self.root)
        UI.filepath.label  = tk.Label(self.root, text="input filepath:  ")
        
            #|get filename
        filepath             = tk.StringVar() #filepath var
        UI.filepath.pickfile = tk.Button(self.root, text = 'Pick file...'
                                , command= lambda: filepath.set(askopenfilename())
                                )
        UI.filepath.entry    = tk.Entry(self.root
                                        , textvariable = filepath
                                       )
            #|datamunging().readTable
        UI.filepath.readfile = tk.Button(self.root, text = 'Read'
                                        , command= lambda:( self.log.delete(1.0, tk.END)
                                                            , datamunging().readTable(filepath.get())
                                                            )
                                        )
        #pack
        UI.filepath.label.pack(in_=UI.filepath.frame, side="left") 
        UI.filepath.entry.pack(in_=UI.filepath.frame, side="left") 
        UI.filepath.pickfile.pack(in_=UI.filepath.frame, side="left")  
        UI.filepath.readfile.pack(in_=UI.filepath.frame, side="left")  
        UI.filepath.frame.pack()   
    
        #+--------------------------------+
        #| * UI elements - transform      |
        #+--------------------------------+  
        # def transform(self, filepath = None, prefix=None, dropchar = 1):
        #+--------------------------------+
        UI.transform = type('transform', (object,), {})
        UI.transform.frame1 = tk.Frame(self.root)  
        UI.transform.frame2 = tk.Frame(self.root)  
        UI.transform.frame3 = tk.Frame(self.root)
        #view
        UI.transform.label1          = tk.Label(self.root, justify='left',text="\
Transform options:\n\n\
If 'hla name' is not provided,\
the second column header \n\
with the last character dropped will be used. \n\n\
Both 'hla name' or '# of drop char' can be overwritten\n")
        UI.transform.label2          = tk.Label(self.root, text="HLA name (optional, string )")
        UI.transform.label3          = tk.Label(self.root, text="drop char(optional, integer)")
        hlaname                      = tk.StringVar()
        dropchar                     = tk.StringVar()
        UI.transform.entry_hlaname   = tk.Entry(self.root, textvariable = hlaname)
        UI.transform.entry_dropchar  = tk.Entry(self.root, textvariable = dropchar)
        UI.transform.transform       = tk.Button(self.root, text = 'transform'
                                        , command = lambda:( self.log.delete(1.0, tk.END)
                                                            ,datamunging().transform(filepath = filepath.get()
                                                                                     , prefix=hlaname.get()
                                                                                     , dropchar=dropchar.get()
                                                                                    )
                                                            )
                                        )#end button
        #pack
        UI.transform.label1.pack(in_=UI.transform.frame1, side="left") 
        UI.transform.frame1.pack()
        
        UI.transform.frame2.pack()        
        UI.transform.label2.pack(in_=UI.transform.frame2, side="left") 
        UI.transform.entry_hlaname.pack(in_=UI.transform.frame2, side="left") 
        
        UI.transform.frame3.pack()
        UI.transform.label3.pack(in_=UI.transform.frame3, side="left") 
        UI.transform.entry_dropchar.pack(in_=UI.transform.frame3, side="left") 
        UI.transform.transform.pack() 
        
        #+--------------------------------+
        #| * UI elements - save      |
        #+--------------------------------+  
        # def wide_to_long(self, filepath, savepath, prefix=None, dropchar = 1):
        #+--------------------------------+
        UI.savepath = type('transform', (object,), {})
        UI.savepath.frame = tk.Frame(self.root)  
        #view
        UI.savepath.frame = tk.Frame(self.root)
        UI.savepath.label  = tk.Label(self.root, text="input savepath:  ")
        
        savepath             = tk.StringVar() #savepath var
        UI.savepath.pickfile = tk.Button(self.root, text = 'Pick folder...'
                                , command= lambda: savepath.set(askdirectory())
                                )
        UI.savepath.entry    = tk.Entry(self.root
                                        , textvariable = savepath
                                       )
            #|datamunging().readTable
        UI.savepath.readfile = tk.Button(self.root, text = 'Save'
                                , command= lambda:(datamunging().wide_to_long(filepath = filepath.get()
                                                                            , savepath = savepath.get()
                                                                            , prefix   = hlaname.get()
                                                                            , dropchar = dropchar.get()
                                                                             )
                                                            )
                                        )
        #pack
        UI.savepath.label.pack(in_=UI.savepath.frame, side="left") 
        UI.savepath.entry.pack(in_=UI.savepath.frame, side="left") 
        UI.savepath.pickfile.pack(in_=UI.savepath.frame, side="left")  
        UI.savepath.readfile.pack(in_=UI.savepath.frame, side="left")  
        UI.savepath.frame.pack()   
            
        
        
        
    def main(self):
        # message box for logging
        log = self.log
        sys.stdout = Redirector(log)
        log.pack()
        
        #clear logs
        clearall = tk.Button(self.root, text = 'clear'
                             , command = lambda: log.delete(1.0, tk.END)
                             )
        clearall.pack()
        # makes gui persistent        
        self.root.mainloop()

App()