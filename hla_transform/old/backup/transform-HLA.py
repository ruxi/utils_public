#!/usr/bin/env python
from __future__ import print_function
import os.path
import sys
import pandas as pd

class datamunging(object):
    def __init__(self):
        pass
    

    def readTable(self, filepath):        
        try:
            open(filepath)
        except IOError:
            print("file does not exist at '" + filepath +"'")
            return
        self.filepath = filepath

        #+------------+
        #| read table |
        #+------------+  
        print('READING TABLE: ' + str(filepath))        
        raw = pd.read_table(filepath)

        #+----------------------------------------+    
        #| categorical datatype for all but col 0 |
        #+----------------------------------------+
        hla_list = list(raw.columns)[1:] 
        for x in hla_list:
            raw[x] = raw[x].astype('category')
    
        #+----------------+
        #| print & return |
        #+----------------+
        print('\nREAD\n'); print(raw.shape); print(raw.head()); print('\n') 
        self.rawdata = raw
        return self
    
    def transform(self, filepath, prefix=None, dropchar = 1):
        self.readTable(filepath)
        raw = self.rawdata   
        #+----------------------+
        #| Exception handling   |
        #+----------------------+  
        if len(dropchar) is 0 or dropchar is None:
            dropchar = 1   
        
        if prefix is None or len(prefix) is 0:
            try:
                prefix = raw.columns[1][:-int(dropchar)]
            except:
                print('invalid raw.col or prefix')
                print('raw.columns[1]', raw.columns[1])
                print('prefix', raw.columns[1][:-dropchar])

        #+------------------------+
        #| split-apply-combine    |
        #+------------------------+ 
        df = pd.melt(raw
                       , id_vars=[raw.columns[0]]
                       , var_name = prefix)


        transformed = df.groupby([raw.columns[0], 'value']).count().unstack().fillna(0)    

        #transformed.columns = transformed.columns.droplevel()
        #return transformed

        #+---------------------------------------+
        #| drop multi-index and append prefix    |
        #+---------------------------------------+         
        # drop multi-level 0
        transformed.columns = transformed.columns.droplevel()
        oldcols = list(transformed.columns)

        # append prefix
        newcols = []
        for x in oldcols:
            new_colname = str(prefix) + "-" + str(x)
            newcols.append(new_colname)

        # replace newcols 
        transformed.columns = newcols
        final = transformed

        print('\nTRANSFORMED\n'); print(final.shape); print(final.head()); 
        self.final = final
        return self.final
    
    def save(self, savepath):
        self.final.to_csv(savepath, sep='\t', encoding='utf-8')
        print('SAVING TO: ' + savepath)
        return self 
    
    def wide_to_long(self, filepath, savepath, prefix=None, dropchar = 1):
        self.transform(filepath, prefix, dropchar)
        self.save(savepath)