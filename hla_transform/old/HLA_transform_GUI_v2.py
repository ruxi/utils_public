
#!/usr/bin/env python
import tkinter as tk  #py3.4
from tkinter.filedialog import askopenfilename, askdirectory
import os.path
import sys
import pandas as pd
class Datamunging(object):
    """
    Updated:  Oct 25, 2017
    Created:  July 22, 2015
              <lewis.r.liu@gmail.com> 
    
    
    This module transforms data from long to wide    
        Before:
            ID       HLA1   HLA2
            sample1  0101   0101
            sample2  0102   0103
            sample3  0101   0102

        After:
            ID       0101   0102  0103
            sample1     2      0     0
            sample2     0      1     1
            sample3     1      1     0
    """
    def __init__(self):
        self.rawdata  = None
        self.filepath = None 

    def readTable(self, filepath, verbose = False):        
        try:
            open(filepath)
        except IOError:
            print("file does not exist at '" + filepath +"'")
            return
        
        self.filepath = filepath
        #+------------+
        #| read table |
        #+------------+  
        print('READING TABLE: ' + str(filepath))    
        
        
        DelimiterOptions = dict(csv = ",", tsv = "\t")
        parsed_ext = os.path.splitext(filepath)[1][1:] 
        if parsed_ext in DelimiterOptions.keys():
            fileext = parsed_ext
        else:
            fileext = 'csv'
            
        raw = pd.read_table(filepath, delimiter = DelimiterOptions[fileext])

        #+----------------------------------------+    
        #| categorical datatype for all but col 0 |
        #+----------------------------------------+
        hla_list = list(raw.columns)[1:] 
        
        ##### pandas 0.15 feature, py2exe conflict 
        for x in hla_list:
            raw[x] = raw[x].astype('category')
    
        #+----------------+
        #| print & return |
        #+----------------+
        if verbose:
            print('\nREAD\n'); print(raw.shape); print(raw.head()); print('\n') 
        self.rawdata = raw
        return self

    @staticmethod
    def _transform(data):
        """transform data from long to wide
        """
        indexcolname = data.columns[0]
        meltdf = pd.melt(data, id_vars = indexcolname, value_vars = data.columns[1:])
        widedata = meltdf.groupby([indexcolname, 'value']).count().unstack().fillna(0)
        widedata.columns = widedata.columns.droplevel(0)
        return widedata
    
    @property
    def transformed(self):
        if self.rawdata is None:
            return "choose filepath for raw data"
        return self._transform(self.rawdata)
    
    def _export(self, data, savepath):
        os.makedirs(os.path.dirname(savepath), exist_ok= True)
        data.to_csv(savepath, encoding='utf-8')
        print('\nsaving to {}'.format(savepath))
        
        
    @property
    def inferred_exportpath(self):
        if self.filepath is None:
            return 'Not Applicable'
        
        workingpath = self.filepath 
        basedir     = os.path.join(os.path.dirname(workingpath), 'output')
        basename    = os.path.basename(workingpath) 
        newname     = 'OUTPUT_widedata_' + basename
        outpath     = os.path.join(basedir, newname)
        return outpath
        
    def export(self, savepath = None):
        if savepath is None:
            savepath = self.inferred_exportpath
        self._export(data = self.transformed, savepath = savepath)
        
#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+
#+--------------------------------------------------------------------------+
       
class Redirector(object):
    """Redirects output from interpreter to GUI"""
    def __init__(self,text_widget):
        self.text_space = text_widget

    def write(self,string):
        self.text_space.insert('end', string)
        self.text_space.see('end')
    
    def flush(self):
        pass  # needed to close app()
              # else 'AttributeError: 'Redirector' object has no attribute 'flush'
    
class App(Datamunging):
    """ GUI interface"""
    def __init__(self):
        self.root = tk.Tk()
        self.log = tk.Text(self.root)
        self.dm  = Datamunging()
        self.GUI()
        self.main()
        
    def GUI(self): 
        self.root.wm_title("Transform HLA data")
        #+-----------------------| 
        #| * Placeholder classes |
        #+-----------------------+
        
        UI = type('UI', (object,), {}) #empty class to hold attributes - http://tinyurl.com/5wkwgnv
        
        #+-----------------------+
        #| * UI elements - head  |
        #+-----------------------+
        
        UI.head = type('head', (object,), {}) #head class to hold UI attributes
        
        UI.head.frame = tk.Frame(self.root)        
        UI.head.frame.pack()
        
        message_main = "{}".format(Datamunging.__doc__)
        UI.head.label = tk.Label(self.root, text=message_main)
        UI.head.label.pack(in_=UI.head.frame, side="left")
                  

        #+--------------------------------+
        #| * UI elements - pick/read file |
        #+--------------------------------+   
        
        UI.filepath = type('filepath', (object,), {}) 
        #view
        UI.filepath.frame = tk.Frame(self.root)
        UI.filepath.label  = tk.Label(self.root, text="input filepath:  ")
        
        #|get filename
        filepath             = tk.StringVar() #filepath var
        UI.filepath.pickfile = tk.Button(self.root, text = 'Pick file...'
                                , command= lambda: filepath.set(askopenfilename())
                                )
        UI.filepath.entry    = tk.Entry(self.root
                                        , textvariable = filepath
                                       )
        #|datamunging().readTable
        UI.filepath.readfile = tk.Button(self.root, text = 'Read'
                                                , command= lambda:( self.log.delete(1.0, tk.END)
                                                                    , self.dm.readTable(filepath.get())
                                                                    , self.log.insert(tk.END, self.dm.rawdata.head())
                                                                    , exportpath.set(self.dm.inferred_exportpath)
                                                            )
                                        )
                                        
        #pack
        UI.filepath.label.pack(in_=UI.filepath.frame, side="left") 
        UI.filepath.entry.pack(in_=UI.filepath.frame, side="left") 
        UI.filepath.pickfile.pack(in_=UI.filepath.frame, side="left")  
        UI.filepath.readfile.pack(in_=UI.filepath.frame, side="left")  
        UI.filepath.frame.pack()  

        #+--------------------------------+
        #| * show transformed             |
        #+--------------------------------+   
        self.log.insert(tk.END, self.dm.transformed)
        UI.transformbutton = tk.Button(self.root, text = 'Show transform'
                                       , command = lambda:(
                                                               self.log.delete(1.0, tk.END)
                                                             , self.log.insert(tk.END, self.dm.transformed)
                                                           )
                                      )
        UI.transformbutton.pack()
        
        ### EXPORT button
        exportpath      = tk.StringVar()
        UI.export = type('export', (object,), {}) 
        UI.export.frame = tk.Frame(self.root)
        UI.export.label = tk.Label(self.root, text = 'export filepath (optional):')
        UI.export.button = tk.Button(self.root, text = 'Export'
                                     , command = lambda: self.dm.export(exportpath.get())
                                    )
        
        
        UI.export.entry    = tk.Entry(self.root
                                        , textvariable = exportpath
                                       )
        exportpath.set(self.dm.inferred_exportpath)
        
        # pack
        UI.export.label.pack(in_=UI.export.frame, side="left")
        UI.export.entry.pack(in_=UI.export.frame, side="left")
        UI.export.button.pack(in_=UI.export.frame, side="left")
        UI.export.frame.pack()

        
    def main(self):
        # message box for logging
        log = self.log
        sys.stdout = Redirector(log)
        log.pack()
        
        #clear logs
        clearall = tk.Button(self.root, text = 'clear'
                             , command = lambda: log.delete(1.0, tk.END)
                             )
        clearall.pack()
        # makes gui persistent        
        self.root.mainloop()

#App()            