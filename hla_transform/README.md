
# README

Last updated: Oct 26, 2017
Created:      July 24, 2015 
    
# DESC

Script to convert HLA data from long to wide. For Chris and Michelle. 

This is a executable file with a GUI interface

# Installation


## OS: linux mint / ubuntu

Give HLA_transform_GUI_v2_OS_linux.sh executable permissions

> ./HLA_transform_GUI_v2_OS_linux.sh 


## OS: windows

TBA

## Other:

Install miniconda: https://conda.io/miniconda.html

> conda env update
> source activate hla
    > python3 HLA_transform_GUI_v2.py